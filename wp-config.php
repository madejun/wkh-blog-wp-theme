<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wkhblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'r00t');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c(F-nFy,RS>2me+2Ovg]HjY6>/z{11kCn=r==@FNxME0mA-k*jajX*{4hU6-?OaQ');
define('SECURE_AUTH_KEY',  '1+@jGNu)[o2vnf03rxkwc${w^;sxc^!(Ea/M#*tS_mfhlyX,{3o}l<k5T[3h3H5B');
define('LOGGED_IN_KEY',    '(27/WF5Is<w4qnCE. W~) [6Y*+]I5y21oZpw}Jk.sBH4#.]tO(AToY>NoQpOqKZ');
define('NONCE_KEY',        '{;k:qg8e}Hl;=.-t7!J).1Up&!nLWISb0eUDeL#s4KQaplXSS~iXt8FKfbx8CZM{');
define('AUTH_SALT',        'Dl*)mM5^.]O|SB<o#R5HuVU[86FeR?J[|B>6CzFv;9~Pzy;nkYLE5oGa!eG4& -y');
define('SECURE_AUTH_SALT', '?!We|Yg(w>ERri~__AnT}q~<r~ky`&#j[(nm_xU1 GOxK^2b!pD F:2rs/*j{)]/');
define('LOGGED_IN_SALT',   'e`:|hG UBkrF?}DV#wg<dXSoCPp*Tdd*;3iFCL0Kq<JqNrhZ7q=E?I#NF4MJ}.a6');
define('NONCE_SALT',       '/JQvS<Ui+Xw2,m=JOd[+#NZ84ZwynqH/|N0( i9D$mnfJLN[HG DLlrz|Q#9pdT8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
