(function() {
    'use strict';

    module.exports = function(grunt) {
        grunt.initConfig({
            clean: {
                //

            },

            watch: {
                less: {
                    options: { livereload: true },
                    files: ["**/*.less"],
                    tasks: ["less"]
                }
            },

            // Compile specified less files
            less: {
                compile: {
                    options: {
                        // These paths are searched for @imports
                        paths: [
                            "less"
                        ]//,
                        // value comments, mediaquery, all.
                        //dumpLineNumbers: 'comments',
                        //
                    },
                    files: {
                        "style.css": "less/theme.less"
                    }
                }
            },


        });

        // Load tasks so we can use them
        grunt.loadNpmTasks("grunt-contrib-watch");
        grunt.loadNpmTasks("grunt-contrib-clean");
        grunt.loadNpmTasks("grunt-contrib-less");

        // The default task will show the usage
        grunt.registerTask("default", "Prints usage", function() {
            grunt.log.writeln("");
            grunt.log.writeln("Tasks");
            grunt.log.writeln("------------------------");
            grunt.log.writeln("");
            grunt.log.writeln("* run 'grunt doless' to automatically build less and watch for its changes");
        });


        grunt.registerTask("doless", [
            "less:compile",
            "watch:less"
        ]);

    };
}());