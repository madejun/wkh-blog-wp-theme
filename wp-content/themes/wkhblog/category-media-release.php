<?php
/**
 * The template for displaying "Media Release" category listing page.
 *
 *
 * @package wkhblog
 */

get_header(); ?>

<?php get_sidebar(); ?>

	<section id="primary" class="content-area col-md-8 pull-right">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php //wkhblog_paging_nav(); 
				wkhblog_numeric_posts_nav();
			?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
