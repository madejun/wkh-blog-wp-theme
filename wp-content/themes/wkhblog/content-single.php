<?php
/**
 * @package wkhblog
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php wkhblog_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( has_post_thumbnail() ) { ?>
		<div class="post-thumbnail">
		<?php	the_post_thumbnail(); ?>
		</div>
		<?php } ?>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wkhblog' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer clearfix">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'wkhblog' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'wkhblog' ) );

			/*if ( ! wkhblog_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wkhblog' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wkhblog' );
				}
			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wkhblog' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'wkhblog' );
				}
			} // end check for categories on this blog */

			/*printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);*/
			if ( '' != $tag_list ) {
				$meta_text = __( '<div class="tags pull-left"><span class="tags-label">Tags</span> %2$s</div>', 'wkhblog' );
			} 
			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>

		<?php edit_post_link( __( 'Edit', 'wkhblog' ), '<span class="edit-link pull-right">', '</span>' ); ?>

	</footer><!-- .entry-footer -->
		<div class="share clearfix">
			<span class="share-label pull-left">Share</span>
			<span class='st_facebook_large st-icon' displayText='Facebook'></span>
			<span class='st_twitter_large st-icon' displayText='Tweet'></span>
			<span class='st_linkedin_large st-icon' displayText='LinkedIn'></span>
			<span class='st_googleplus_large st-icon' displayText='Google +'></span>
			<a href="<?php _e( 'mailto:?subject=Check out this site&amp;body=Check out this site '.get_permalink(), 'wkhblog'); ?>" class="share-to-email">Email</a>
		</div>
</article><!-- #post-## -->
