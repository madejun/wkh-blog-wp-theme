<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package wkhblog
 */
?>

	</div></div></div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container container-940">
			<div class="site-info pull-left">
				<div class="footer-link">
					<a href="http://wealth-knowhow.com.au/pages/term-conditions" target="_blank">Terms &amp; Conditions</a>
					<span class="sep"> | </span>
					<a href="http://wealth-knowhow.com.au/pages/privacy-policy" target="_blank">Privacy Policy</a>
				</div>
				<div class="copyright">
					&copy; 2013 Chip Chat &amp; Chew PTY 
				</div>
			</div>

			<div class="social-media pull-right">
				<a href="http://www.linkedin.com/company/wealth-know-how?goback=%2Enmp_*1_*1_*1_*1_*1_*1_*1_*1_*1_*1&trk=top_nav_home" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-linkedin-footer.jpg" alt="linkedin"></a>
				<a href="https://twitter.com/wealthknowhow" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-twitter-footer.jpg" alt="twitter"></a>
				<a href="https://plus.google.com/112879620922325469297/about" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-googleplus-footer.jpg" alt="google plus"></a>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
