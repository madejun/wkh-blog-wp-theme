<?php
/**
 * wkhblog functions and definitions
 *
 * @package wkhblog
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'wkhblog_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wkhblog_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wkhblog, use a find and replace
	 * to change 'wkhblog' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wkhblog', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wkhblog' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	/*add_theme_support( 'custom-background', apply_filters( 'wkhblog_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );*/
}
endif; // wkhblog_setup
add_action( 'after_setup_theme', 'wkhblog_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wkhblog_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wkhblog' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'wkhblog_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wkhblog_scripts() {
	wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css' );
	wp_enqueue_style( 'source-sans-pro-fonts', 'http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,400italic');
	wp_enqueue_style( 'wkhblog-style', get_stylesheet_uri() );

	wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', array(), '20140917', true );
	wp_enqueue_script( 'wkhblog-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'wkhblog-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/script.js', array(), '20140928', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wkhblog_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function wkhblog_search_form( $form ) {
	$form = '<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '">
			<label class="screen-reader-text">' . __( 'Search for:', 'label' ) . '</label>
			<input type="search" class="search-field pull-left" placeholder="' . __( 'Search our blog', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" />
			<input type="submit" class="search-submit" value="'. esc_attr__( 'Search', 'submit button' ) .'" />
			</form>';

	return $form;
}

add_filter( 'get_search_form', 'wkhblog_search_form' );

function wkhblog_load_jquery() {
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'wkhblog_load_jquery');

function wkhblog_excerpt_more($more) {
    global $post;
    if(!is_search()){
		return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read more <span class="arrow"></span></a>';
	}
}
add_filter('excerpt_more', 'wkhblog_excerpt_more');

add_theme_support( 'post-thumbnails' ); 

function wkhblog_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li class="prev">%s</li>' . "\n", get_previous_posts_link('PREV') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li class="next">%s</li>' . "\n", get_next_posts_link('NEXT') );

	echo '</ul></div>' . "\n";

}

function wkhblog_custom_archive($args = '') {
    global $wpdb, $wp_locale;

    $defaults = array(
        'limit' => '',
        'format' => 'html', 'before' => '',
        'after' => '', 'show_post_count' => false,
        'echo' => 1
    );

    $r = wp_parse_args( $args, $defaults );
    extract( $r, EXTR_SKIP );

    if ( '' != $limit ) {
        $limit = absint($limit);
        $limit = ' LIMIT '.$limit;
    }

    // over-ride general date format ? 0 = no: use the date format set in Options, 1 = yes: over-ride
    $archive_date_format_over_ride = 0;

    // options for daily archive (only if you over-ride the general date format)
    $archive_day_date_format = 'Y/m/d';

    // options for weekly archive (only if you over-ride the general date format)
    $archive_week_start_date_format = 'Y/m/d';
    $archive_week_end_date_format   = 'Y/m/d';

    if ( !$archive_date_format_over_ride ) {
        $archive_day_date_format = get_option('date_format');
        $archive_week_start_date_format = get_option('date_format');
        $archive_week_end_date_format = get_option('date_format');
    }

    //filters
    $where = apply_filters('customarchives_where', "WHERE post_type = 'post' AND post_status = 'publish'", $r );
    $join = apply_filters('customarchives_join', "", $r);

    $output = '<div class="panel-group archive-accordion" id="archive-accordion">
  				<div class="panel panel-default">
    				<div class="panel-heading panel-heading-main">
      					<div class="panel-title">
        					<a data-toggle="collapse" class="collapsed" data-parent="#archive-accordion" href="#collapseOne">
          						Select Archive <span class="caret"></span>
						    </a>
						</div>
					</div>
				<div id="collapseOne" class="panel-collapse collapse">
					<div class="panel-body">';


        $query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC $limit";
        $key = md5($query);
        $cache = wp_cache_get( 'wkhblog_custom_archive' , 'general');
        if ( !isset( $cache[ $key ] ) ) {
            $arcresults = $wpdb->get_results($query);
            $cache[ $key ] = $arcresults;
            wp_cache_set( 'wkhblog_custom_archive', $cache, 'general' );
        } else {
            $arcresults = $cache[ $key ];
        }
        if ( $arcresults ) {
            $afterafter = $after;
            
            $counter = 0; $counterMonth = 0; 
            foreach ( (array) $arcresults as $arcresult ) {
                $url = get_month_link( $arcresult->year, $arcresult->month );
                /* translators: 1: month name, 2: 4-digit year */
                $text = sprintf(__('%s'), $wp_locale->get_month($arcresult->month));
                $year_text = sprintf('%d', $arcresult->year);
                if ( $show_post_count )
                    $after = '&nbsp;('.$arcresult->posts.')' . $afterafter;
                
                if($arcresult->year != $temp_year){
                	$counterMonth = 0;
                }else{
                	$counterMonth++;
                }

                if($counterMonth == 0){
                	if($counter != 0) {
                		$output .= '</div>
			    				</div>
					  		</div>
						</div>';
                	}

                	$output .= '<div class="panel-group archive-year-accordion" id="archive-'. sprintf('%d', $arcresult->year) .'-accordion">
  									<div class="panel panel-default">
						    			<div class="panel-heading">
						      				<div class="panel-title">
						        				<a data-toggle="collapse" class="collapsed" data-parent="#archive-'. sprintf('%d', $arcresult->year) .'-accordion" href="#collapse-'. sprintf('%d', $arcresult->year) .'">
						          					<span class="caret"></span>'.$year_text.
						          				'</a>
											</div>
										</div>
										<div id="collapse-'. sprintf('%d', $arcresult->year) .'" class="panel-collapse collapse">
											<div class="panel-body"><ul>';
                }
                $output .= get_archives_link($url, $text, $format, $before, $after);
                
                $temp_year = $arcresult->year;
                $counter++;
            }  
			$output .= '</div>
			    		</div>
			  		</div>
				</div>';
        }

    $output .= 	'</div>
    		</div>
  		</div>
	</div>';

    if ( $echo )
        echo $output;
    else
        return $output;
}

function wkhblog_highlight_results($text){
     if(is_search()){
     $sr = get_query_var('s');
     $keys = explode(" ",$sr);
     $text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">'.$sr.'</strong>', $text);
     }
     return $text;
}
add_filter('the_excerpt', 'wkhblog_highlight_results');
add_filter('the_title', 'wkhblog_highlight_results');