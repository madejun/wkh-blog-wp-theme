<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package wkhblog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "ur-a748b72f-e7d3-d666-ea9-f031e50742ff", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wkhblog' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
	  <div class="container container-940">
	  	<a class="mobile-menu-btn" href="#">menu</a>

		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		</div>

        <div class="pull-right right-wrap">
            <?php get_search_form( ); ?>
            <a class="search-link-mobile" href="<?php echo esc_url( home_url( '/' ) ).'search/'; ?>"></a>
            <a class="login-link header-custom-link pull-left" href="http://wealth-knowhow.com.au/users/sign_in" target="_blank">LOGIN</a>
            <a class="join-link header-custom-link pull-left" href="http://wealth-knowhow.com.au/users/sign_up" target="_blank">JOIN</a>
        </div>

	<?/*	<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle"><?php _e( 'Primary Menu', 'wkhblog' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation --> */ ?>
      </div>
	</header><!-- #masthead -->

	<div id="content" class="site-content"><div class="container container-940"><div class="row">
