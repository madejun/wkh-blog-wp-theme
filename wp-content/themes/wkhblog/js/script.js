( function( $ ) {
	$('a[href$=".pdf"]').addClass('pdf-file');

	$('.mobile-menu-btn').on('click', function(){
		$('body').toggleClass('slide-open');
		return false;
	});

	$('a img').each(function(){
		$(this).closest('a').addClass('no-opaque');
	});

	var bodyHeight = $('body').height();	
	var vwptHeight = $(window).height();
	if (vwptHeight > bodyHeight) {
		$('.site-footer').css({
			'bottom': '0',
			'left': '0',
			'position': 'absolute',
			'right': '0'
		});
	}

	$(window).on('resize', function () {
		var bodyHeight = $('body').height();	
		var vwptHeight = $(window).height();
		if (vwptHeight > bodyHeight) {
			$('.site-footer').css({
				'bottom': '0',
				'left': '0',
				'position': 'absolute',
				'right': '0'
			});
		}
	});

	if( $('#secondary').children().length > 0 ){
		$('.site-content').prepend('<div class="pseudo-sidebar" />');
		$('.pseudo-sidebar').height($(window).height());
	}

} )( jQuery );