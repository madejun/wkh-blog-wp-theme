<?php
/*
Template Name: Search Page
*/

/**
 * The template for displaying search results pages.
 *
 * @package wkhblog
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="search-result-wrapper">

			<header class="page-header">
				<?php get_search_form(); ?>

				
			</header><!-- .page-header -->


			</div>

			<?php //wkhblog_paging_nav(); 
				wkhblog_numeric_posts_nav();
			?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
