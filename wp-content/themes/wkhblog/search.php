<?php
/**
 * The template for displaying search results pages.
 *
 * @package wkhblog
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="search-result-wrapper">

			<header class="page-header">
				<?php get_search_form(); ?>
				<h1 class="page-title"><?php printf( __( 'You searched for "%s"', 'wkhblog' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				
				<?php global $wp_query; 
					  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;  
					  $post_per_page = get_query_var( 'posts_per_page' );
					  $currently_max = ( ($paged*$post_per_page) < $wp_query->found_posts ) ? ($paged*$post_per_page) : $wp_query->found_posts;
					  $currently_min = ($paged*$post_per_page) - $post_per_page + 1;
				?>

				<p class="search-result-count">Results <?php echo $currently_min; ?>-<?php echo $currently_max; ?> of about <?php echo $wp_query->found_posts; ?></p>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'content', 'search' );
				?>

			<?php endwhile; ?>

			</div>

			<?php //wkhblog_paging_nav(); 
				wkhblog_numeric_posts_nav();
			?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
