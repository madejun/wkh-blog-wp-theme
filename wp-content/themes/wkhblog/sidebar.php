<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package wkhblog
 */

/*if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}*/
?>

<div id="secondary" class="widget-area col-md-4" role="complementary">
	<a href="http://wealth-knowhow.com.au/users/sign_in" target="_blank" class="login-link-mobile">Login or Join</a>

<?php if(is_home() || is_category('media-release')) { ?>
	<a class="blog-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Blog</a>

	<?php
	$term = term_exists('Media Release', 'category');
	if ($term !== 0 && $term !== null) {
	?>
	<a class="media-release-link" href="<?php echo esc_url( home_url( '/' ) ).'category/media-release/'; ?>">Media Releases</a>
	<?php
	}
	?>

	<?php  if (function_exists('wkhblog_custom_archive')) wkhblog_custom_archive(); ?>
<?php } ?>

<?php if(is_single() && !in_category('media-release')) { ?>
	<a class="blog-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><span class="caret"></span>Blog</a>
<?php } ?>

<?php if(in_category('media-release') && !is_category('media-release') && is_single()) { ?>
	<?php
	$term = term_exists('Media Release', 'category');
	if ($term !== 0 && $term !== null) {
	?>
	<a class="media-release-link single" href="<?php echo esc_url( home_url( '/' ) ).'category/media-release/'; ?>" rel="home"><span class="caret"></span>Media Releases</a>
	<?php
	}
	?>
<?php } ?>

	<?php dynamic_sidebar( 'sidebar-1' ); ?>

	<div class="footer-mobile">
		<div class="footer-link">
			<a href="http://wealth-knowhow.com.au/pages/term-conditions" target="_blank">Terms</a>
			<span class="sep"> | </span>
			<a href="http://wealth-knowhow.com.au/pages/privacy-policy" target="_blank">Privacy</a>
		</div>
		<div class="copyright">
			&copy; 2013 Chip Chat &amp; Chew PTY 
		</div>
	</div>
</div><!-- #secondary -->
